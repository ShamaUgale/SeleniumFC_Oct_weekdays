package dropdowns;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class selectTag {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {

		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Desktop\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.navigate().to("https://www.commonfloor.com/agent/login?show_signup=1");

		WebElement cityDD=driver.findElement(By.id("city"));
		
		
		Select sec= new Select(cityDD);
		
		// use case 1 -- selct an elem in dd
		sec.selectByIndex(87);
		
		Thread.sleep(1000);
		
		
		sec.selectByValue("Mumbai"); // attribute value
		Thread.sleep(1000);
		
		
		sec.selectByVisibleText("Kolkata");
		
		
		
		WebElement elem =sec.getFirstSelectedOption();
		
		// use case 2 -- fetch wat elem is selected from the dd
		System.out.println(elem.getText());
		
		// count the no of elem in dd
		
		List<WebElement> cites=sec.getOptions();
		
		System.out.println("No of cities : " + cites.size());
		
		Iterator<WebElement> it= cites.iterator();
		
		while(it.hasNext()){
			System.out.println(it.next().getText());
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
