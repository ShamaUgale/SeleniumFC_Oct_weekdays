package frames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class w3schools {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.navigate().to("http://www.w3schools.com/html/tryit.asp?filename=tryhtml_default");

		// identify the frame
		
	System.out.println("No of frames on the page "+driver.findElements(By.tagName("iframe")).size());
		
		//switch to the frame -- using index
//		driver.switchTo().frame(2);
	
//	driver.switchTo().frame("iframeResult");// id or name 
		
		WebElement frame= driver.findElement(By.xpath("//*[@id='iframeResult']"));
		
		driver.switchTo().frame(frame);
		// this elem is in a frame
		String txt= driver.findElement(By.xpath("/html/body/h1")).getText();
		
		System.out.println(txt);
	}

}
