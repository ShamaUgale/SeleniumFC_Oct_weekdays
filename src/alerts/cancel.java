package alerts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class cancel {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver_win32 (3)\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.navigate().to("http://www.javascriptkit.com/javatutors/alert2.shtml");

		
		driver.findElement(By.name("B3")).click();
		
		Alert alt = driver.switchTo().alert();
		String msg=alt.getText();
		Thread.sleep(2000);
		System.out.println(msg);
		
		alt.dismiss(); // clicks on Cancel/NO
		
	}

}
