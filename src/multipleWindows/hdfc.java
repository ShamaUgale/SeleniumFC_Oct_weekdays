package multipleWindows;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class hdfc {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\shama.ugale\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.navigate().to("http://www.hdfcbank.com/");
		
		Thread.sleep(5000);
		System.out.println("************* before clicking on login btn *************");
		
		Set<String> winIds=driver.getWindowHandles();
		
		System.out.println(winIds);
		// click on login btn- which opens up in the new window
		driver.findElement(By.id("loginsubmit")).click();
		winIds=driver.getWindowHandles();
		System.out.println("************* after clicking on login btn *************");

		System.out.println(winIds);
		
		Iterator<String> it= winIds.iterator();
		String parentWin= it.next();
		String loginWin= it.next();
		
		
		
		System.out.println("I am on :--> " + driver.getTitle());
		
		driver.switchTo().window(loginWin);
		
		System.out.println("I am on :--> " + driver.getTitle());

		driver.switchTo().window(parentWin);
		
		System.out.println("I am on :--> " + driver.getTitle());
		
		WebElement applyMenu=driver.findElement(By.xpath("//span[text()='Apply now']"));
		
		Actions act= new Actions(driver);
		act.moveToElement(applyMenu).build().perform();
		Thread.sleep(3000);
		
		//clicks on car loan
		driver.findElement(By.xpath("//a[text()='Car Loan']")).click();
		Thread.sleep(2000);
		
		winIds=driver.getWindowHandles();
		it= winIds.iterator();
		 parentWin= it.next();
		 loginWin= it.next();
		String carLoanWin=it.next();
		
		driver.switchTo().window(carLoanWin);
		
		System.out.println("I am on :--> " + driver.getTitle());
		
		
		
		
		
		
		
	}

}
